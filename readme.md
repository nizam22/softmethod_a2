**System Specification**

In this image processing python project I have converted an input image to grayscale and display it first. Then I applied the gaussian filter in both directions. The ultimate goal is to produce a scaled image from the generated gaussian image.

**User input:** An image

**System output:** Scaled Gausian filtered image

Here i have used several python library- PIL, pylab, numpy, scipy
