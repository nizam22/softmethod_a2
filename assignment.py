from PIL import Image
from pylab import *
from numpy import *
from scipy.ndimage import filters




# Converting the image to grey scale and displaying it
def GreyScale(image):
    imageG = image.convert('L')
    return imageG

# Applying gaussian filter
def gaussian(image):
    
    # working in x direction
    image1 = filters.gaussian_filter(image, 3,(1,0))
    imageX = uint8(image1)
    #imageX = Image.fromarray(imageX, 'L')
    
    #Working in y direction
    image2 = filters.gaussian_filter(image, 2,(0,1))
    imageY = uint8(image2)
    #imageY = Image.fromarray(imageY, 'L')

    #Adding for both direction
    imageXY = imageX + imageY
    return imageXY


# Scalling
def scalling(image):

    # Scalling between 0 and 255
    scaled = interp(image, (image.min(), image.max()), (0, 255))
    
    # Writing out the final grayscale output as an image
    Final_Image = Image.fromarray(scaled.astype(uint8), 'L')
    return Final_Image


#opening and displaying the image
cl = Image.open("ullafayette.jpg") 
cl.show()

GreyImage = GreyScale(cl)
GreyImage.show()

imageXY= gaussian(GreyImage)

Final_Image = scalling(imageXY)
Final_Image.show()
